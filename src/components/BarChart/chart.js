import React from "react";
import { Bar } from "@nivo/bar";

// import data from '../../data'
import axios from 'axios';
import './chart.css'
import { throws } from "assert";

const styles = {
    fontFamily: "sans-serif",
    textAlign: "center"    
};


class Chart extends React.Component {

    constructor() {
        super()
        this.state = {            
            data: []
        }        
    }

    componentDidMount() {
        const url = 'https://ucdoecphh2.execute-api.us-east-1.amazonaws.com/dev/segment'
        
        return axios(url, {
            method: 'POST',                                       
            data: { 
                "event": "Billed",
                "from_date": "2019-12-01",
                "to_date": "2019-12-12",
                "group_by_key": "company.name" 
            }
          }).then(res => {
            const payload = res.data;
            console.log(payload);            
            this.setState({                 
                data: payload.data
            });
          }).catch( error => {
            console.error(error)
          })
    }

    render() {
        return (
            <div className="chart" style={styles}>                
                <Bar
                width={600}
                height={400}
                margin={{ top: 60, right: 80, bottom: 60, left: 80 }}
                data={this.state.data}
                indexBy="key"
                keys={["value"]}                
                margin={{ top: 50, right: 60, bottom: 50, left: 120 }}
                pixelRatio={2}
                padding={0.15}
                innerPadding={0}
                minValue="auto"
                maxValue="auto"
                groupMode="stacked"
                layout="horizontal"
                reverse={false}
                colors={{ scheme: 'set2' }}
                colorBy="id"
                borderWidth={0}
                borderColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                axisTop={{ tickSize: 5, tickPadding: 5, tickRotation: 0, legend: '', legendOffset: 36 }}
                axisRight={null}
                axisBottom={{
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: '',
                    legendPosition: 'middle',
                    legendOffset: 36
                }}
                axisLeft={{
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: '',
                    legendPosition: 'middle',
                    legendOffset: -40
                }}
                enableGridX={true}
                enableGridY={false}
                enableLabel={true}
                labelSkipWidth={12}
                labelSkipHeight={12}
                labelTextColor={{ from: 'color', modifiers: [ [ 'darker', 1.6 ] ] }}
                isInteractive={true}
                />
            </div>
        )
    }
}

export default Chart